package com.example.android.weatherwithsqlite;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by hessro on 2/24/18.
 */

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SQLiteDatabase mDB;
    //private SavedForecastAdapter.ForecastItem mSavedForecastItem;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EditTextPreference locationPref = (EditTextPreference)findPreference(getString(R.string.pref_location_key));
        locationPref.setSummary(locationPref.getText());

        OWMSavedDBHelper dbHelper = new OWMSavedDBHelper((SettingsActivity)getActivity());
        mDB = dbHelper.getWritableDatabase();

        Preference clearButton = findPreference(getString(R.string.clear_db_key));
        clearButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder alertBuilder;
                alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setTitle("Clear Saved Locations")
                        .setMessage("Are you sure you want to delete your location history?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearDB();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
                return true;
            }
        });
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_location_key))) {
            EditTextPreference locationPref = (EditTextPreference)findPreference(key);
            locationPref.setSummary(locationPref.getText());
            if (!checkIsLocationSaved()) {
                addLocationDataToDB();
            }
        }
    }

    @Override
    public void onResume() {
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mDB.close();
        super.onDestroy();
    }

    // All of the saving to the database happens here
    private boolean checkIsLocationSaved() {
        boolean isSaved = false;

        EditTextPreference locationPref = (EditTextPreference)findPreference(getString(R.string.pref_location_key));
        String locationString = locationPref.getText();

        if (locationString != null) {
            String sqlSelection = OWMContract.SavedLocations.COLUMN_LOCATION_NAME + " = ?";
            String[] sqlSelectionArgs = { locationString };
            Cursor cursor = mDB.query(
                    OWMContract.SavedLocations.TABLE_NAME,
                    null,
                    sqlSelection,
                    sqlSelectionArgs,
                    null,
                    null,
                    null
            );
            isSaved = cursor.getCount() > 0;
            cursor.close();
        }

        if (isSaved) {
            Log.d("SettingsFrag", "Existing location found: " + locationString);
        }
        return isSaved;
    }

    private long addLocationDataToDB() {
        EditTextPreference locationPref = (EditTextPreference)findPreference(getString(R.string.pref_location_key));
        String locationString = locationPref.getText();

        Log.d("SettingsFrag", "Adding to database: " + locationString);

        if (locationString != null) {
            ContentValues row = new ContentValues();
            row.put(OWMContract.SavedLocations.COLUMN_LOCATION_NAME, locationString);
            return mDB.insert(OWMContract.SavedLocations.TABLE_NAME, null, row);
        } else {
            return -1;
        }
    }

    private void clearDB() {
        Log.d("SettingsFrag", "Nuking everything...");
        mDB.execSQL("DELETE FROM " + OWMContract.SavedLocations.TABLE_NAME + ";");
    }

}
