package com.example.android.weatherwithsqlite;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.weatherwithsqlite.utils.OpenWeatherMapUtils;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by victorli on 2018/03/10.
 */

public class SavedForecastAdapter extends RecyclerView.Adapter<SavedForecastAdapter.ForecastItemViewHolder> {

    public static class ForecastItem implements Serializable {
        public String locationName;

        public String getText() {
            return locationName;
        }
    }

    private ArrayList<ForecastItem> mForecastItems;
    private OnForecastItemClickListener mForecastItemClickListener;
    private Context mContext;

    public interface OnForecastItemClickListener {
        void onSavedForecastItemClick(String forecastLocation);
    }

    public SavedForecastAdapter(Context context, OnForecastItemClickListener clickListener) {
        mContext = context;
        mForecastItemClickListener = clickListener;
        Log.d("SavedForecastAdapter","Creation!");
    }

    public void updateForecastItems(ArrayList<SavedForecastAdapter.ForecastItem> forecastItems) {
        mForecastItems = forecastItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mForecastItems != null) {
            return mForecastItems.size();
        } else {
            return 0;
        }
    }

    @Override
    public ForecastItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.saved_forecast_item, parent, false);
        return new SavedForecastAdapter.ForecastItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ForecastItemViewHolder holder, int position) {
        holder.bind(mForecastItems.get(position));
    }

    class ForecastItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mSavedForecastLocation;

        public ForecastItemViewHolder(View itemView) {
            super(itemView);
            mSavedForecastLocation = itemView.findViewById(R.id.tv_saved_forecast_location);
            itemView.setOnClickListener(this);
        }

        public void bind(ForecastItem locationItem) {
            mSavedForecastLocation.setText(locationItem.getText());
        }

        @Override
        public void onClick(View v) {
            mForecastItemClickListener.onSavedForecastItemClick((String)mSavedForecastLocation.getText());
        }
    }
}
