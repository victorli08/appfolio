package com.example.android.weatherwithsqlite;

import android.provider.BaseColumns;

/**
 * Created by victorli on 2018/03/10.
 */

public class OWMContract {
    private OWMContract() {

    }

    public static class SavedLocations implements BaseColumns {
        public static final String TABLE_NAME = "savedForecastLocationsTable";
        public static final String COLUMN_LOCATION_NAME = "locationName";
        public static final String COLUMN_TIMESTAMP = "timestamp";
    }
}
