package com.example.android.weatherwithsqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by victorli on 2018/03/10.
 */

public class OWMSavedDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "owmSavedLocations.db";
    private static int DATABASE_VERSION = 1;

    public OWMSavedDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_SAVED_LOCATIONS_TABLE =
                "CREATE TABLE " + OWMContract.SavedLocations.TABLE_NAME + "(" +
                        OWMContract.SavedLocations.COLUMN_LOCATION_NAME + " TEXT NOT NULL, " +
                        OWMContract.SavedLocations.COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" +
                        ");";

        db.execSQL(SQL_CREATE_SAVED_LOCATIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + OWMContract.SavedLocations.TABLE_NAME + ";");
        onCreate(db);
    }
}
