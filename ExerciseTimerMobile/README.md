# ExerciseTimerMobile
On Start                   | During Warm Up            | During Cool Down
:-------------------------:|:-------------------------:|:-------------------------:
<img src="images/main.png" class="imgarray" alt="main" width="250"/>  |  <img src="images/warmup.png" class="imgarray" alt="ppage" width="250"/> | <img src="images/cooldown.png" class="imgarray" alt="ppage" width="250"/>

An iOS conversion of the ExerciseTimer macOS app.

Features:
- Timer
- 3 workout presets (more to come, and eventually customizability)
- Pause and continue workout from where you left off at any time
- Options to manually end workout or go straight to cool down
- Restart functionality that allows you to string multiple workouts together on the same timer*

*Note: Obviously, it's important to know your limits. Don't exercise too much for too long, we're not responsible for your injuries!

---
