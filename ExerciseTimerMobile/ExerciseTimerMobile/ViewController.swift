//
//  ViewController.swift
//  ExerciseTimerMobile
//
//  Created by Victor Li on 3/29/19.
//  Copyright © 2019 Victor Li. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    // link all of the UI fields before so we have access to them later
    @IBOutlet weak var dateIndicator: UITextField!
    
    @IBOutlet weak var workoutTimerDisplay: UITextField!
    
    @IBOutlet weak var phaseIndicator: UITextField!
    
    @IBOutlet weak var exerciseIndicator: UITextField!
    
    @IBOutlet weak var modeSelector: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateIndicator.text = dateFormatter.string(from:Date())
        
        workoutTimerDisplay.text = "--:--:--"
        phaseIndicator.text = "---"
        exerciseIndicator.text = "---"
    }

    var timerActive = false
    var timerPaused = false
    var exerciseTimer = Timer()
    var seconds = 0
    var phaseStartSeconds = 0
    var phase = ""
    var pausedPhase = ""
    var prevColor = UIColor.black
    
    var currentExercise = ""
    var reps = 0
    var prevReps = 0
    let coreReps = 9
    
    var stretches = ["Overhead Stretch", "Tricep Right Stretch", "Tricep Left Stretch", "Arm Shoulder Left Stretch", "Arm Shoulder Right Stretch", "Right Quad Stretch", "Left Quad Stretch", "Right Calf Stretch", "Left Calf Stretch", "Hamstring Stretch Right", "Hamstring Stretch Left", "Glute Stretch Right", "Glute Stretch Left"]
    
    let synth = AVSpeechSynthesizer()
    var u = AVSpeechUtterance(string: "")
    
    var workoutMode = 0
    
    @IBAction func pauseButton(_ sender: UIButton) {
        if !timerPaused && timerActive {
            exerciseTimer.invalidate()
            pausedPhase = phaseIndicator.text!
            timerPaused = true
            timerActive = false
            prevColor = phaseIndicator.textColor ?? UIColor.black
            phaseIndicator.textColor = UIColor.purple
            phaseIndicator.text = "Paused"
        }
    }
    
    @IBAction func startButton(_ sender: UIButton) {
        u = AVSpeechUtterance(string: "Session Begin!")
        u.voice = AVSpeechSynthesisVoice(language: "en-AU")
        synth.speak(u)
        
        dateIndicator.textColor = UIColor.blue
        workoutMode = modeSelector.selectedSegmentIndex
        if (timerActive == false) {
            exerciseTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            timerActive = true
            timerPaused = false
        }
        
        phase = ""
    }
    
    @objc func timerAction() {
        seconds += 1
        workoutTimerDisplay.text = String(format: "%02d", seconds/3600) + ":" + String(format: "%02d", seconds/60) + ":" + String(format: "%02d", seconds%60)
        
        // do any time checking, exercise changing, and dictation here!
        if workoutMode == 0 {
            if phase == "" {
                phaseStartSeconds = seconds
                phase = "warm up"
                phaseIndicator.textColor = UIColor.orange
                phaseIndicator.text = "Warm Up!"
                exerciseIndicator.text = "Warm Up!"
                u = AVSpeechUtterance(string: "Warm Up Begin!")
                u.voice = AVSpeechSynthesisVoice(language: "en-AU")
                synth.speak(u)
            }
            else if phase == "warm up" {
                if ((0 ... 60) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.text = "Jumping Jacks!"
                }
                else if ((61 ... 90) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.text = "Rest!"
                }
                else if ((91 ... 180) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.text = "Light Jog!"
                }
                else if ((181 ... 240) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.text = "Rest!"
                }
                else {
                    phase = "core"
                    phaseIndicator.text = "Running!"
                    phaseStartSeconds = seconds
                }
            }
            else if phase == "core" {
                if reps < coreReps {
                    if ((0 ... 60) ~= (seconds - phaseStartSeconds)) {
                        exerciseIndicator.text = "Power Walk!"
                    }
                    else if ((61 ... 120) ~= (seconds - phaseStartSeconds)) {
                        exerciseIndicator.text = "Jog!"
                    }
                    else if ((121 ... 180) ~= (seconds - phaseStartSeconds)) {
                        exerciseIndicator.text = "Sprint!"
                    }
                    else if ((seconds - phaseStartSeconds) == 181) {
                        phaseStartSeconds = seconds
                        reps += 1
                    }
                }
                else if reps == coreReps {
                    reps = coreReps
                    phaseStartSeconds = seconds
                    phase = "cool down"
                    phaseIndicator.text = "Cool Down!"
                    synth.speak(AVSpeechUtterance(string: "Cool Down Begin!"))
                }
            }
            else if phase == "cool down" {
                if reps < stretches.count {
                    exerciseIndicator.text = stretches[reps] + "!"
                    if ((seconds - phaseStartSeconds) == 50) {
                        phaseStartSeconds = seconds
                        reps += 1
                    }
                }
                else if reps == stretches.count {
                    reps = 0
                    phase = ""
                    phaseIndicator.textColor = UIColor.blue
                    phaseIndicator.text = "Complete!"
                    exerciseIndicator.text = "Rest!"
                    exerciseTimer.invalidate()
                    pausedPhase = ""
                    timerPaused = false
                    timerActive = false
                    prevColor = UIColor.black
                }
            }
        }
    }
    
    @IBAction func continueButton(_ sender: UIButton) {
        if timerPaused && !timerActive {
            timerPaused = false
            timerActive = true
            exerciseTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            phaseIndicator.textColor = prevColor
            phaseIndicator.text = pausedPhase
        }
    }
    
    @IBAction func coolButton(_ sender: UIButton) {
        if !timerPaused && timerActive {
            reps = 0
            phaseStartSeconds = seconds
            phase = "cool down"
            phaseIndicator.text = "Cool Down!"
            u = AVSpeechUtterance(string: "Cool Down Begin!")
            u.voice = AVSpeechSynthesisVoice(language: "en-AU")
            synth.speak(u)
        }
    }
    
    @IBAction func endButton(_ sender: UIButton) {
        reps = 0
        phase = ""
        phaseIndicator.textColor = UIColor.orange
        phaseIndicator.text = "Manually Ended"
        exerciseTimer.invalidate()
        pausedPhase = ""
        timerPaused = false
        timerActive = false
        prevColor = UIColor.black
    }
    
    func speak() {
        if currentExercise != exerciseIndicator.text {
            u = AVSpeechUtterance(string: exerciseIndicator.text!)
            synth.speak(u)
            currentExercise = exerciseIndicator.text!
        }
    }
}

