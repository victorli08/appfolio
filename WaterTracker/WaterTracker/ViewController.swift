//
//  ViewController.swift
//  WaterTracker
//
//  Created by Victor Li on 4/3/19.
//  Copyright © 2019 Mesarthim. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var selectMilliliter: NSButton!
    @IBOutlet weak var selectOunce: NSButton!
    @IBOutlet weak var measurementIndicator: NSTextField!
    @IBOutlet weak var ouncesDrankIndicator: NSTextField!
    @IBOutlet weak var waterEntryField: NSTextField!
    @IBOutlet weak var dateIndicator: NSTextField!
    
    var selectedOunces = true
    var ouncesDrank = 0.0
    
    let mlPerOz = 29.57
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "MMMM d, yyyy"
        dateIndicator.stringValue = dateFormatter.string(from:Date())
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func selectMeasure(sender: NSButton) {
        if selectMilliliter.state == .on {
            selectedOunces = false
        }
        else if selectOunce.state == .on {
            selectedOunces = true
        }
        updateFields()
    }
    
    func updateFields() {
        if selectedOunces {
            measurementIndicator.stringValue = "fl. oz"
            ouncesDrankIndicator.stringValue = String(ouncesDrank)
        }
        else {
            measurementIndicator.stringValue = "mL"
            ouncesDrankIndicator.stringValue = String(format: "%.2f", (ouncesDrank*mlPerOz))
        }
    }
    
    @IBAction func addWater(_ sender: NSButton) {
//        if (let entryData = waterEntryField.stringValue) {
        if (waterEntryField.stringValue.isDouble()) {
            if selectedOunces {
                ouncesDrank += Double(waterEntryField.stringValue)!
            }
            else {
                ouncesDrank += (Double(waterEntryField.stringValue)!*mlPerOz)
            }
        }
        else {
            let badInputTypeAlert = NSAlert()
            badInputTypeAlert.messageText = "Invalid Number"
            badInputTypeAlert.informativeText = "Please enter a valid whole or decimal number."
            badInputTypeAlert.beginSheetModal(for: self.view.window!) { (response) in

            }
        }
        updateFields()
    }
//    }
}

// extend string with checker functions
extension String {
    func isInt() -> Bool {
        if let intValue = Int(self) {
            if intValue >= 0 {
                return true
            }
        }
        return false
    }
    
    func isDouble() -> Bool {
        if let doubleValue = Double(self) {
            if doubleValue >= 0 {
                return true
            }
        }
        return false
    }
}
