# battlecrypokedex
Main Activity              | Pokemon Activity  
:-------------------------:|:-------------------------:
<img src="images/main.png" class="imgarray" alt="main" width="250"/>  |  <img src="images/ppage.png" class="imgarray" alt="ppage" width="250"/>

An experimental first-generation Pokedex using PokeAPI v2, a RESTful API by Paul Hallet. See [pokeapi](https://pokeapi.co) for more on PokeAPI.

Optimized for Android 8.0.0 Oreo.

App co-creators:
- [Austin Kwong](https://github.com/kwongar)
- [Monica Sek](https://github.com/mssek)

See our original project [here](https://github.com/OSU-CS492-W18/final-project-team-rocket) on the OSU Winter 2018 CS492 final project repo.

Complete features:
- Playable battle cry sounds for each Pokemon.
- Pokemon type display, ability display, and evolution diagram.
- Share icon opens the Pokemon's corresponding page on the Pokemon wiki.

Incomplete features:
- Who's That Pokemon? (Quiz game; the app plays a sound and players must guess the corresponding Pokemon.)
- Caching to minimize evolution data fetch overhead.

---

__FAIR USE NOTICE:__

"Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use."

This app is made using Pokemon content solely for educational purposes and is NOT COMMERCIALIZED IN ANY WAY.
