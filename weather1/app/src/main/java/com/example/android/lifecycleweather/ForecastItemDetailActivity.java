package com.example.android.lifecycleweather;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.android.lifecycleweather.data.WeatherPreferences;
import com.example.android.lifecycleweather.utils.OpenWeatherMapUtils;

import java.text.SimpleDateFormat;

public class ForecastItemDetailActivity extends AppCompatActivity {

    private static final String FORECAST_HASHTAG = "#CS496Weather";
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(WeatherPreferences.getDefaultDateFormat());

    private TextView mDateTV;
    private TextView mTempDescriptionTV;
    private TextView mLowHighTempTV;
    private TextView mWindTV;
    private TextView mHumidityTV;
    private OpenWeatherMapUtils.ForecastItem mForecastItem;

    private Context appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_item_detail);

        mDateTV = (TextView)findViewById(R.id.tv_date);
        mTempDescriptionTV = (TextView)findViewById(R.id.tv_temp_description);
        mLowHighTempTV = (TextView)findViewById(R.id.tv_low_high_temp);
        mWindTV = (TextView)findViewById(R.id.tv_wind);
        mHumidityTV = (TextView)findViewById(R.id.tv_humidity);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(OpenWeatherMapUtils.ForecastItem.EXTRA_FORECAST_ITEM)) {
            mForecastItem = (OpenWeatherMapUtils.ForecastItem)intent.getSerializableExtra(
                    OpenWeatherMapUtils.ForecastItem.EXTRA_FORECAST_ITEM
            );
            fillInLayoutText(mForecastItem);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.forecast_item_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                shareForecast();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void shareForecast() {
        if (mForecastItem != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String tempUnitsAbbr = "", tempUnits = sharedPreferences.getString("pref_temp_units","imperial");
            if (!TextUtils.isEmpty(tempUnits)) {
                if (tempUnits.equals("imperial")) {
                    tempUnitsAbbr = "F";
                } else if(tempUnits.equals("metric")) {
                    tempUnitsAbbr = "C";
                }
            } else {
                tempUnitsAbbr = "K";
            }

            String shareText = "Check out " + sharedPreferences.getString("pref_city_editable", "")
                    + ", " + sharedPreferences.getString("pref_country_editable","") + "'s weather! Expecting "
                    + mForecastItem.description + " at " + mForecastItem.dateTime
                    + " UTC, with a temperature of " + mForecastItem.temperature + tempUnitsAbbr + " and "
                    + mForecastItem.humidity + "% humidity.";

            ShareCompat.IntentBuilder.from(this)
                    .setType("text/plain")
                    .setText(shareText)
                    .setChooserTitle(R.string.share_chooser_title)
                    .startChooser();
        }
    }

    private void fillInLayoutText(OpenWeatherMapUtils.ForecastItem forecastItem) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String tempUnitsAbbr = "", tempUnits = sharedPreferences.getString("pref_temp_units","imperial");
        if (!TextUtils.isEmpty(tempUnits)) {
            if (tempUnits.equals("imperial")) {
                tempUnitsAbbr = "F";
            } else if(tempUnits.equals("metric")) {
                tempUnitsAbbr = "C";
            }
        } else {
            tempUnitsAbbr = "K";
        }

        String dateString = DATE_FORMATTER.format(forecastItem.dateTime);
        String detailString = forecastItem.temperature +
                tempUnitsAbbr +
                " - " + forecastItem.description;
        String lowHighTempString = "Low: " + forecastItem.temperatureLow +
                tempUnitsAbbr +
                "   High: " + forecastItem.temperatureHigh +
                tempUnitsAbbr;
        String windString = "Wind: " + forecastItem.windSpeed + " MPH " + forecastItem.windDirection;
        String humidityString = "Humidity: " + forecastItem.humidity + "%";

        mDateTV.setText(dateString);
        mTempDescriptionTV.setText(detailString);
        mLowHighTempTV.setText(lowHighTempString);
        mWindTV.setText(windString);
        mHumidityTV.setText(humidityString);
    }
}
