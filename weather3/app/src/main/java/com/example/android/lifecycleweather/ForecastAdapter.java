package com.example.android.lifecycleweather;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.lifecycleweather.data.WeatherPreferences;
import com.example.android.lifecycleweather.utils.OpenWeatherMapUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static com.example.android.lifecycleweather.OpenWeatherMapsLoader.TAG;

/**
 * Created by hessro on 5/10/17.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastItemViewHolder> {

    private ArrayList<OpenWeatherMapUtils.ForecastItem> mForecastItems;
    private OnForecastItemClickListener mForecastItemClickListener;

    private Context appContext;

    public interface OnForecastItemClickListener {
        void onForecastItemClick(OpenWeatherMapUtils.ForecastItem forecastItem);
    }

    public ForecastAdapter(OnForecastItemClickListener clickListener, Context context) {
        mForecastItemClickListener = clickListener;
        appContext = context;
    }

    public void updateForecastItems(ArrayList<OpenWeatherMapUtils.ForecastItem> forecastItems) {
        mForecastItems = forecastItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mForecastItems != null) {
            return mForecastItems.size();
        } else {
            return 0;
        }
    }

    @Override
    public ForecastItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.forecast_item, parent, false);
        return new ForecastItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ForecastItemViewHolder holder, int position) {
        holder.bind(mForecastItems.get(position));
    }

    class ForecastItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mForecastDateTV;
        private TextView mForecastTempDescriptionTV;
        private final SimpleDateFormat dateFormatter;

        public ForecastItemViewHolder(View itemView) {
            super(itemView);
            dateFormatter = new SimpleDateFormat(WeatherPreferences.getDefaultDateFormat());
            mForecastDateTV = (TextView)itemView.findViewById(R.id.tv_forecast_date);
            mForecastTempDescriptionTV = (TextView)itemView.findViewById(R.id.tv_forecast_temp_description);
            itemView.setOnClickListener(this);
        }

        public void bind(OpenWeatherMapUtils.ForecastItem forecastItem) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
            String tempUnitsAbbr = "", tempUnits = sharedPreferences.getString("pref_temp_units","imperial");
            if (!TextUtils.isEmpty(tempUnits)) {
               if (tempUnits.equals("imperial")) {
                   tempUnitsAbbr = "F";
               } else if(tempUnits.equals("metric")) {
                   tempUnitsAbbr = "C";
               }
            } else {
                tempUnitsAbbr = "K";
            }

            Log.d(TAG, "units: " + tempUnits);

            String dateString = dateFormatter.format(forecastItem.dateTime);

            String detailString = forecastItem.temperature +
                    tempUnitsAbbr +
                    " - " + forecastItem.description;
            mForecastDateTV.setText(dateString);
            mForecastTempDescriptionTV.setText(detailString);
        }

        @Override
        public void onClick(View v) {
            OpenWeatherMapUtils.ForecastItem forecastItem = mForecastItems.get(getAdapterPosition());
            mForecastItemClickListener.onForecastItemClick(forecastItem);
        }
    }
}
