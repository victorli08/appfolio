package com.example.android.lifecycleweather;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.AsyncTaskLoader;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.android.lifecycleweather.data.WeatherPreferences;
import com.example.android.lifecycleweather.utils.NetworkUtils;
import com.example.android.lifecycleweather.utils.OpenWeatherMapUtils;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements ForecastAdapter.OnForecastItemClickListener, LoaderManager.LoaderCallbacks<String>, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private final static int WEATHER_LOADER_ID = 0;
    private final static String WEATHER_LOADER_URL_KEY = "OWMSearchURL";

    private TextView mForecastLocationTV;
    private EditText mEntryForecastLocationET;
    private RecyclerView mForecastItemsRV;
    private ProgressBar mLoadingIndicatorPB;
    private TextView mLoadingErrorMessageTV;
    private ForecastAdapter mForecastAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Remove shadow under action bar.
        getSupportActionBar().setElevation(0);

        mForecastLocationTV = (TextView)findViewById(R.id.tv_forecast_location);
        mForecastLocationTV.setText(WeatherPreferences.getDefaultForecastLocation());

        mEntryForecastLocationET = (EditText)findViewById(R.id.location_search_edit_text);

        mLoadingIndicatorPB = (ProgressBar)findViewById(R.id.pb_loading_indicator);
        mLoadingErrorMessageTV = (TextView)findViewById(R.id.tv_loading_error_message);
        mForecastItemsRV = (RecyclerView)findViewById(R.id.rv_forecast_items);

        mForecastAdapter = new ForecastAdapter(this,this);
        mForecastItemsRV.setAdapter(mForecastAdapter);
        mForecastItemsRV.setLayoutManager(new LinearLayoutManager(this));
        mForecastItemsRV.setHasFixedSize(true);

        getSupportLoaderManager().initLoader(WEATHER_LOADER_ID, null, this);

        Button refreshButton = (Button)findViewById(R.id.refresh_btn);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadForecast();
            }
        });

        loadForecast();
    }

    @Override
    public void onForecastItemClick(OpenWeatherMapUtils.ForecastItem forecastItem) {
        Intent intent = new Intent(this, ForecastItemDetailActivity.class);
        intent.putExtra(OpenWeatherMapUtils.ForecastItem.EXTRA_FORECAST_ITEM, forecastItem);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                showForecastLocation();
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, PreferencesActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // changes to do here:
    // - use preferences to pass stringized arguments into the weather builder
    // - take the opportunity to update the UI!
    public void loadForecast() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

//        String locationCity = sharedPreferences.getString("pref_city_editable", "Corvallis");
        String locationCity = mEntryForecastLocationET.getText().toString();

        String locationCountry = sharedPreferences.getString("pref_country_editable", "US");
        String weatherUnits = sharedPreferences.getString("pref_temp_units","imperial");

        mForecastLocationTV.setText("\n " + locationCity + ", " + locationCountry + "\n");

        String openWeatherMapForecastURL = OpenWeatherMapUtils.buildForecastURL(locationCity, locationCountry, weatherUnits);

        Bundle args = new Bundle();
        args.putString(WEATHER_LOADER_URL_KEY, openWeatherMapForecastURL);

        //new OpenWeatherMapForecastTask().execute(openWeatherMapForecastURL);

        Log.d(TAG, "got forecast url: " + openWeatherMapForecastURL);
        mLoadingIndicatorPB.setVisibility(View.VISIBLE);
        getSupportLoaderManager().restartLoader(WEATHER_LOADER_ID, args, this);
    }

    public void showForecastLocation() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//        String locationCity = sharedPreferences.getString("pref_city_editable", "Corvallis");
        String locationCountry = sharedPreferences.getString("pref_country_editable", "US");

        String locationCity = mEntryForecastLocationET.getText().toString();

        Uri geoUri = Uri.parse("geo:0,0").buildUpon()
                .appendQueryParameter("q", locationCity + ", " + locationCountry)
                .build();
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        String OWMURL = null;
        if(args != null) {
            OWMURL = args.getString(WEATHER_LOADER_URL_KEY);
        }
        return new OpenWeatherMapsLoader(this, OWMURL);
    }

    @Override
    public void onLoadFinished(Loader loader, String data) {
        mLoadingIndicatorPB.setVisibility(View.INVISIBLE);
        Log.d(TAG, "OWM loading complete");
        if (data != null) {
            ArrayList<OpenWeatherMapUtils.ForecastItem> OWMResults = OpenWeatherMapUtils.parseForecastJSON(data);
            mForecastAdapter.updateForecastItems(OWMResults);
            mLoadingErrorMessageTV.setVisibility(View.INVISIBLE);
            mForecastItemsRV.setVisibility(View.VISIBLE);
        } else {
            mForecastItemsRV.setVisibility(View.INVISIBLE);
            mLoadingErrorMessageTV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.app_pref_city_key))) {
            loadForecast();
        }
        else if (key.equals(getString(R.string.app_pref_country_key))) {
            loadForecast();
        }
        else if (key.equals(getString(R.string.app_pref_temp))) {
            loadForecast();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }
}
