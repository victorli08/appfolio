package com.example.android.lifecycleweather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by victorli on 2018/02/25.
 */

public class PreferencesActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
    }
}
