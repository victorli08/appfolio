package com.example.android.lifecycleweather;

import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.io.IOException;

import com.example.android.lifecycleweather.utils.NetworkUtils;

/**
 * Created by victorli on 2018/02/25.
 */

public class OpenWeatherMapsLoader extends AsyncTaskLoader<String> {
    public final static String TAG = OpenWeatherMapsLoader.class.getSimpleName();

    String mOWMResults;
    String mOWMFetchURL;

    OpenWeatherMapsLoader(Context context, String url) {
        super(context);
        mOWMFetchURL = url;
    }

    @Override
    protected void onStartLoading() {
        if (mOWMFetchURL != null) {
            if (mOWMResults != null) {
                Log.d(TAG, "returning cached OWM results");
                deliverResult(mOWMResults);
            } else {
                forceLoad();
            }
        }
    }

    @Override
    public String loadInBackground() {
        if (mOWMFetchURL != null) {
            Log.d(TAG, "loading weather data from OWM API using URL: " + mOWMFetchURL);
            String searchResults = null;
            try {
                searchResults = NetworkUtils.doHTTPGet(mOWMFetchURL);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return searchResults;
        } else {
            return null;
        }
    }

    @Override
    public void deliverResult(String data) {
        mOWMResults = data;
        super.deliverResult(data);
    }
}
