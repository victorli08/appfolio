package com.example.android.lifecycleweather;

import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.content.SharedPreferences;

/**
 * Created by victorli on 2018/02/25.
 */

public class PreferencesFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EditTextPreference cityPref = (EditTextPreference)findPreference("pref_city_editable");
        cityPref.setSummary(cityPref.getText());
        EditTextPreference countryPref = (EditTextPreference)findPreference("pref_country_editable");
        countryPref.setSummary(countryPref.getText());
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.app_pref_city_key))) {
            EditTextPreference cityPref = (EditTextPreference)findPreference("pref_city_editable");
            cityPref.setSummary(cityPref.getText());
        }
        else if (key.equals(getString(R.string.app_pref_country_key))) {
            EditTextPreference countryPref = (EditTextPreference)findPreference("pref_country_editable");
            countryPref.setSummary(countryPref.getText());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
