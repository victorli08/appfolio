//
//  ViewController.swift
//  ExerciseTimer
//
//  Created by Victor Li on 3/1/19.
//  Copyright © 2019 Victor Li. All rights reserved.
//

import Cocoa
import AVFoundation

class ViewController: NSViewController {
    
    // all fields
    @IBOutlet weak var dateIndicator: NSTextField!
    @IBOutlet weak var modeSelectorField: NSPopUpButton!
    @IBOutlet weak var workoutTimerDisplay: NSTextField!
    @IBOutlet weak var phaseIndicator: NSTextField!
    @IBOutlet weak var exerciseIndicator: NSTextField!
    @IBOutlet weak var modeSelector: NSPopUpButton!
    @IBOutlet weak var muteStatus: NSTextField!
    
    // voice variables and constants
    let synth = NSSpeechSynthesizer()
    var muteVoice = false
    
    // timer variables
    var timerActive = false
    var timerPaused = true
    var accel = false
    var exerciseTimer = Timer()
    var seconds = 0
    
    // mode variables
    var workoutMode = 0
    var warmUp = true
    var stretchOnWarmup = true
    var strengthOnCooldown = true
    
    // workout variables
    var phaseStartSeconds = 0
    var phase = ""
    var pausedPhase = ""
    var prevColor = NSColor.white
    var currentExercise = ""
    var reps = 0
    var prevReps = 0
    var eP = 0
    var coreReps = 0
    
    // workout constants
    let restTime = 68
    let timeOn = 47
    let timeOff = 21
    
    // workout presets
    var stretchSet = ["Overhead Stretch", "Tricep Right Stretch", "Tricep Left Stretch", "Arm Shoulder Left Stretch", "Arm Shoulder Right Stretch", "Right Quad Stretch", "Left Quad Stretch", "Right Calf Stretch", "Left Calf Stretch", "Hamstring Stretch Right", "Hamstring Stretch Left", "Crunches", "Sit Ups", "Glute Stretch Right", "Glute Stretch Left"]
    var stretches = ["Overhead Stretch", "Tricep Right Stretch", "Tricep Left Stretch", "Arm Shoulder Left Stretch", "Arm Shoulder Right Stretch", "Right Quad Stretch", "Left Quad Stretch", "Right Calf Stretch", "Left Calf Stretch", "Hamstring Stretch Right", "Hamstring Stretch Left", "Crunches", "Sit Ups", "Glute Stretch Right", "Glute Stretch Left"]
    var exercises = ["Jumping Jacks", "Knee Ups", "Star Jumps", "Butt Kicks", "Skater Jumps", "Jump Rope Jacks", "Squat Jacks", "Fake Run", "Jump Squats"]
    var activeRestExercises = ["Jump Rope Jacks", "Butt Kicks", "Knee-Ups", "Fake Run", "Squats"]
    
    // customizable workout structs
    struct exercise {
        var name = ""
        var active : Int = 30
        var rest : Int = 30
    }
    
    struct workoutRoutine {
        // two options: supply global times or supply individual times
        // global times will simply loop through each exercise set with a preset active/rest time
        var restTime = 0
        var onTime = 0
        var offTime = 0
        var warmUpSetStrings = [""]
        var primarySetStrings = [""]
        var coolDownSetStrings = [""]
        
        // individual times allow each exercise to be precisely specified
        // each exercise will have a name as well as an active/rest time
        var warmUpSetExercises : [exercise]
        var primarySetExercises : [exercise]
        var coolDownSetExercises : [exercise]
    }
    
    // overrides
    override func viewDidAppear() {
        super.viewDidAppear()
        self.view.window?.title = "Exercise Timer"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "MMMM d, yyyy"
        dateIndicator.stringValue = dateFormatter.string(from:Date())
        
        modeSelectorField.addItems(withTitles: ["Plyo Only", "Active Rest"])
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    // control button actions
    @IBAction func startButton(_ sender: NSButton) {
        announce(utterance: "Session Begin!")
        
        dateIndicator.textColor = NSColor.green
        workoutMode = modeSelector.indexOfSelectedItem
        if (timerActive == false) {
            startTimer()
            timerActive = true
            timerPaused = false
        }
        
        phase = ""
    }

    @IBAction func pauseButton(_ sender: NSButton) {
        if !timerPaused && timerActive {
            exerciseTimer.invalidate()
            pausedPhase = phaseIndicator.stringValue
            timerPaused = true
            prevColor = phaseIndicator.textColor ?? NSColor.white
            phaseChange(phaseNameExternal: "Paused", phaseTextColor: NSColor.purple)
        }
    }
    
    @IBAction func continueButton(_ sender: NSButton) {
        if timerPaused && timerActive {
            timerPaused = false
            startTimer()
            phaseChange(phaseNameExternal: pausedPhase, phaseTextColor: prevColor)
        }
    }
    
    @IBAction func stopButton(_ sender: NSButton) {
        if timerActive {
            phaseChange(phaseNameExternal: "Manually Ended", phaseNameInternal: "", phaseTextColor: NSColor.orange)
            exerciseIndicator.stringValue = "---"
            workoutFinishTeardown()
        }
    }
    
    @IBAction func coolButton(_ sender: NSButton) {
        if !timerPaused && timerActive {
            eP = 0
            reps = 0
            phaseChange(phaseNameExternal: "Cool Down!", phaseNameInternal: "cool down", phaseTextColor: NSColor.yellow)
            announce(utterance: "Cool Down Begin!")
        }
    }
    
    @IBAction func resetButton(_ sender: NSButton) {
        if !timerActive {
            seconds = 0
            workoutTimerDisplay.stringValue = "00:00:00"
        }
    }
    
    // check boxes
    @IBAction func stretchSelect(_ sender: NSButton) {
        if sender.state == .on {
            stretchOnWarmup = true
        }
        else {
            stretchOnWarmup = false
        }
    }
    
    @IBAction func coreSelect(_ sender: NSButton) {
        if sender.state == .on {
            strengthOnCooldown = true
        }
        else {
            strengthOnCooldown = false
        }
    }
    
    @IBAction func muteCheck(_ sender: NSButton) {
        if sender.state == .on {
            muteVoice = true
        }
        else {
            muteVoice = false
        }
    }
    
    @IBAction func warmUpCheck(_ sender: NSButton) {
        if sender.state == .on {
            warmUp = true
        }
        else {
            warmUp = false
        }
    }
    
    @IBAction func accelCheck(_ sender: NSButton) {
        if sender.state == .on {
            accel = true
        }
        else {
            accel = false
        }
    }
    
    func warmUpRoutine() {
        var warmUpReps = 0
        if stretchOnWarmup {
            warmUpReps = stretches.count
        }
        
        if reps < warmUpReps {
            exerciseIndicator.stringValue = stretches[reps] + "!"
            if ((seconds - phaseStartSeconds) == 30) {
                resetInterphaseTracker()
                reps += 1
            }
        }
        else if reps == warmUpReps {
            reps += 1
            resetInterphaseTracker()
        }
        else if reps == (warmUpReps + 1) {
            if ((0*restTime ... restTime) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Jumping Jacks!"
            }
            else if ((restTime+1 ... restTime*2) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((restTime*2)+1 ... restTime*3) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Light Jog!"
            }
            else if (((restTime*3)+1 ... restTime*4) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else {
                phase = "core"
                reps = 0
                resetInterphaseTracker()
            }
        }
    }
    
    func coolDown() {
        let coreMoves = ["Crunches", "Sit Ups"]
        
        var secondsPerStretch = 0
        if stretchOnWarmup {
            secondsPerStretch = 30
        }
        else {
            secondsPerStretch = 50
        }
        
        if !strengthOnCooldown {
            stretches.removeAll()
            for i in (0 ... stretchSet.count-1) {
                if !(coreMoves.contains(stretchSet[i])) {
                    stretches.append(stretchSet[i])
                }
            }
        }
        else {
            stretches = stretchSet
        }
        
        if reps < stretches.count {
            exerciseIndicator.stringValue = stretches[reps] + "!"
            if ((seconds - phaseStartSeconds) == secondsPerStretch) {
                resetInterphaseTracker()
                reps += 1
            }
        }
        else if reps == stretches.count {
            phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "", phaseTextColor: NSColor.green)
            exerciseIndicator.stringValue = "Rest!"
            currentExercise = "Rest!"
            announce(utterance: "Cool Down End, Workout Completed!")
            workoutFinishTeardown()
        }
    }
    
    func workoutFinishTeardown() {
        eP = 0
        reps = 0
        exerciseTimer.invalidate()
        
        pausedPhase = ""
        prevColor = NSColor.white
        
        timerPaused = false
        timerActive = false
    }
    
    // functions for each workout preset
    func run1() {
        phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "na", phaseTextColor: NSColor.yellow)
        if reps < coreReps {
            if eP < 3 {
                if ((0 ... restTime) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Power Walk!"
                }
                else if ((restTime+1 ... restTime*2) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Jog!"
                }
                else if (((restTime*2)+1 ... restTime*3) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Run!"
                }
                else {
                    resetInterphaseTracker()
                    eP += 1
                }
            }
            else if eP == 3 {
                if ((0 ... restTime) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Rest!"
                }
                else {
                    resetInterphaseTracker()
                    eP = 0
                    reps += 1
                }
            }
        }
        else if reps == coreReps {
            reps = 0
            resetInterphaseTracker()
            phaseChange(phaseNameExternal: "Cool Down!", phaseNameInternal: "cool down", phaseTextColor: NSColor.yellow)
            announce(utterance: "Cool Down Begin!")
            updateTextFieldWithTime(field: dateIndicator)
        }
    }
    
    func run2() {
        phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "na", phaseTextColor: NSColor.yellow)
        if reps < coreReps {
            if ((0 ... restTime) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Jog!"
            }
            else if ((restTime+1 ... restTime*2) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Run!"
            }
            else if (((restTime*2)+1 ... restTime*3) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Jog!"
            }
            else if (((restTime*3)+1 ... restTime*4) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Run!"
            }
            else if (((restTime*4)+1 ... restTime*5) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Jog!"
            }
            else if ((seconds - phaseStartSeconds) == (restTime*5)+1) {
                resetInterphaseTracker()
                reps += 1
            }
        }
        else if reps == coreReps {
            reps = 0
            resetInterphaseTracker()
            phaseChange(phaseNameExternal: "Cool Down!", phaseNameInternal: "cool down", phaseTextColor: NSColor.yellow)
            announce(utterance: "Cool Down Begin!")
            updateTextFieldWithTime(field: dateIndicator)
        }
    }
    
    func run3M() {
        if (eP < exercises.count) {
            phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "na", phaseTextColor: NSColor.yellow)
            if ((0 ... timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = exercises[eP] + "!"
            }
            else if ((timeOn+1 ... (timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((timeOn+timeOff)+1 ... (timeOn+timeOff)+timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = exercises[eP] + "!"
            }
            else if (((timeOn+timeOff)+1+timeOn ... 2*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((2*(timeOn+timeOff))+1 ... (2*(timeOn+timeOff))+timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = exercises[eP] + "!"
            }
            else if (((2*(timeOn+timeOff))+1+timeOn ... 3*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((3*(timeOn+timeOff))+1 ... 4*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Jog!"
            }
            else if (((4*(timeOn+timeOff))+1 ... 5*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Run!"
            }
            else if (((5*(timeOn+timeOff))+1 ... 6*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Jog!"
            }
            else if (((6*(timeOn+timeOff))+1 ... 7*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else {
                resetInterphaseTracker()
                eP += 1
            }
        }
        else if eP == exercises.count {
            reps = 0
            eP += 1
        }
        else if eP > exercises.count {
            reps = 0
            resetInterphaseTracker()
            phaseChange(phaseNameExternal: "Cool Down!", phaseNameInternal: "cool down", phaseTextColor: NSColor.yellow)
            announce(utterance: "Cool Down Begin!")
            updateTextFieldWithTime(field: dateIndicator)
        }
    }
    
    func plyoOnly() {
        phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "na", phaseTextColor: NSColor.yellow)
        if (eP < exercises.count) {
            if ((0 ... timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = exercises[eP] + "!"
            }
            else if ((timeOn+1 ... (timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((timeOn+timeOff)+1 ... (timeOn+timeOff)+timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = exercises[eP] + "!"
            }
            else if (((timeOn+timeOff)+1+timeOn ... 2*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((2*(timeOn+timeOff))+1 ... (2*(timeOn+timeOff))+timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = exercises[eP] + "!"
            }
            else if (((2*(timeOn+timeOff))+1+timeOn ... 3*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else {
                resetInterphaseTracker()
                eP += 1
            }
        }
        else if eP == exercises.count {
            reps = 0
            resetInterphaseTracker()
            phaseChange(phaseNameExternal: "Cool Down!", phaseNameInternal: "cool down", phaseTextColor: NSColor.yellow)
            announce(utterance: "Cool Down Begin!")
            updateTextFieldWithTime(field: dateIndicator)
        }
    }
    
    func activeRest() {
        phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "na", phaseTextColor: NSColor.yellow)
        if (eP < activeRestExercises.count) {
            if ((0 ... timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = activeRestExercises[eP] + "!"
            }
            else if ((timeOn+1 ... (timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((timeOn+timeOff)+1 ... (timeOn+timeOff)+timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = activeRestExercises[eP] + "!"
            }
            else if (((timeOn+timeOff)+1+timeOn ... 2*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else if (((2*(timeOn+timeOff))+1 ... (2*(timeOn+timeOff))+timeOn) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = activeRestExercises[eP] + "!"
            }
            else if (((2*(timeOn+timeOff))+1+timeOn ... 3*(timeOn+timeOff)) ~= (seconds - phaseStartSeconds)) {
                exerciseIndicator.stringValue = "Rest!"
            }
            else {
                resetInterphaseTracker()
                eP += 1
            }
        }
        else if eP == activeRestExercises.count {
            reps = 0
            eP += 1
        }
        else if eP > activeRestExercises.count {
            phaseChange(phaseNameExternal: modeSelector.titleOfSelectedItem!, phaseNameInternal: "na", phaseTextColor: NSColor.yellow)
            if reps < coreReps {
                if ((0 ... restTime) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Power Walk!"
                }
                else if ((restTime+1 ... restTime*2) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Jog!"
                }
                else if (((restTime*2)+1 ... restTime*3) ~= (seconds - phaseStartSeconds)) {
                    exerciseIndicator.stringValue = "Run!"
                }
                else if ((seconds - phaseStartSeconds) == (restTime*3)+1) {
                    resetInterphaseTracker()
                    reps += 1
                }
            }
            else if reps == coreReps {
                reps = 0
                resetInterphaseTracker()
                phaseChange(phaseNameExternal: "Cool Down!", phaseNameInternal: "cool down", phaseTextColor: NSColor.yellow)
                announce(utterance: "Cool Down Begin!")
                updateTextFieldWithTime(field: dateIndicator)
            }
        }
    }
    
    func executeWorkoutFromTemplate_Strings(workoutTemplate : workoutRoutine?) {
        if workoutRoutine! != nil {
            
        }
    }
    
    func executeWorkoutFromTemplate_Exercises(workoutTemplate : workoutRoutine?) {
        if workoutRoutine! != nil {
            
        }
    }
    
    // internal functions
    func phaseChange(phaseNameExternal: String, phaseNameInternal: String = "na", phaseTextColor: NSColor) -> Void {
        phaseIndicator.textColor = phaseTextColor
        phaseIndicator.stringValue = phaseNameExternal
        if !(phaseNameInternal == "na") {
            phase = phaseNameInternal
        }
    }
    
    func updateTextFieldWithTime(field: NSTextField) -> Void {
        field.stringValue = String(format: "%02d", seconds/3600) + ":" + String(format: "%02d", (seconds/60)%60) + ":" + String(format: "%02d", seconds%60)
    }
    
    // timer functions
    func startTimer() {
        if accel {
            exerciseTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
        else {
            exerciseTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
    }
    
    @objc func timerAction() {
        seconds += 1
        updateTextFieldWithTime(field: workoutTimerDisplay)
        
        // do any time checking, exercise changing, and dictation here!
        if phase == "" {
            resetInterphaseTracker()
            if warmUp {
                phaseChange(phaseNameExternal: "Warm Up!", phaseNameInternal: "warm up", phaseTextColor: NSColor.yellow)
                exerciseIndicator.stringValue = "Warm Up!"
                announce(utterance: "Warm Up Begin!")
            }
            else {
                phase = "core"
            }
        }
        else if phase == "warm up" {
            warmUpRoutine()
        }
        else if phase == "core" {
            if modeSelector.indexOfSelectedItem == 0 {
                coreReps = 3
                run1()
            }
            else if modeSelector.indexOfSelectedItem == 1 {
                coreReps = 9
                run2()
            }
            else if modeSelector.indexOfSelectedItem == 2 {
                coreReps = 3
                run3M()
            }
            else if modeSelector.indexOfSelectedItem == 3 {
                coreReps = 0
                plyoOnly()
            }
            else if modeSelector.indexOfSelectedItem == 4 {
                coreReps = 2
                activeRest()
            }
        }
        else if phase == "cool down" {
            coolDown()
        }
        
        speak()
    }
    
    func resetInterphaseTracker() -> Void {
        phaseStartSeconds = seconds
    }
    
    // voice functions
    func announce(utterance: String) -> Void {
        if !muteVoice {
            synth.startSpeaking(utterance)
        }
    }
    
    func speak() {
        if !muteVoice {     // speak function will only trigger a voiceover if changing exercise/tas
            if currentExercise != exerciseIndicator.stringValue {
                announce(utterance: exerciseIndicator.stringValue)
                currentExercise = exerciseIndicator.stringValue
            }
        }
    }
}
