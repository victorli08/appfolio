# ExerciseTimer
On Start                   | During Warm Up            | During Cool Down
:-------------------------:|:-------------------------:|:-------------------------:
<img src="images/main.png" class="imgarray" alt="main" width="250"/>  |  <img src="images/warmup.png" class="imgarray" alt="ppage" width="250"/> | <img src="images/cooldown.png" class="imgarray" alt="ppage" width="250"/>

A little prototype NASA/ISS-style workout tool designed to time treadmill/exercise bike sessions for macOS.

Features:
- Timer for recording time active and total workout time
- 5 workout presets, including a setting for active rest days
- Pause and continue workout from where you left off at any time
- Options to manually end workout or go straight to cool down
- Restart functionality that allows you to string multiple workouts together*

*Note: Obviously, it's important to know your limits. Don't exercise too much for too long, we're not responsible for your injuries!

---

Upcoming Features:
- Workout History Log
- 3 customizable Presets
